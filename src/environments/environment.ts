// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig : {
    apiKey: "AIzaSyArWcyNeWK6OsQVvHmSCfyWSo8bTkRdN7E",
    authDomain: "example11oryan.firebaseapp.com",
    projectId: "example11oryan",
    storageBucket: "example11oryan.appspot.com",
    messagingSenderId: "474431126700",
    appId: "1:474431126700:web:6f6983bdea44ad01d8b309"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
