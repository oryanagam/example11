import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth.service';
import { Item } from '../interfaces/item';
import { ItemsService } from '../items.service';



@Component({
  selector: 'app-items',
  templateUrl: './items.component.html',
  styleUrls: ['./items.component.css']
})
export class ItemsComponent implements OnInit {

  items$; 
  items:Item[];
  userId:string; 
  checked = false;
  isInInventory:boolean;

  save(item:Item){
    this.itemsService.save(this.userId,item.id ,item.name, item.price,item.inventory,item.ST=2);
  }

  unsave(item:Item){
    this.itemsService.save(this.userId,item.id ,item.name, item.price,item.inventory,item.ST=0);
  }

  ininventory(item:Item){
    this.itemsService.ininventory(this.userId,item.id ,item.name, item.price,item.inventory=true ,item.ST);
  }


  deleteItem(id:string){
    this.itemsService.deleteItem(this.userId,id); 
  }

  constructor(private itemsService:ItemsService, public authService:AuthService) { }

  ngOnInit(): void {
    this.authService.getUser().subscribe(
      user => {
        this.userId = user.uid;
        console.log(this.userId); 
        this.items$ = this.itemsService.getItems(this.userId); 
        
        this.items$.subscribe(
          docs =>{
            console.log('init worked');           
            this.items = [];
            for(let document of docs){
              const item:Item = document.payload.doc.data();
              item.id = document.payload.doc.id; 
              this.items.push(item); 
            }
          }
        ) 
      }
    )

  }

}
