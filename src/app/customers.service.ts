import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import {AngularFirestore, AngularFirestoreCollection} from '@angular/fire/firestore';
import { map } from 'rxjs/operators';


@Injectable({
  providedIn: 'root'
})
export class CustomersService {

  customers = [
  {name:'Rachel',years:'12',income:'20K'},
  {name:'Yakir',years:'12',income:'20k'}
];

  public addCustomers(){
    setInterval(()=>this.customers.push({name:'Customers Name',years:'years of study',income:'monthly income'}),2000);
  }

  customerCollection:AngularFirestoreCollection;
  userCollection:AngularFirestoreCollection = this.db.collection('users'); 

  public getCustomers(userId){
    this.customerCollection = this.db.collection(`users/${userId}/customers`); 
    return this.customerCollection.snapshotChanges()      
  } 

  save(userId:string,id:string,name:string,years:number,income:number,category:string,ST=2){
    this.db.doc(`users/${userId}/customers/${id}`).update(
      {
        name:name,
        years:years,
        income:income,
        category:category,
        ST:ST
      }
    )
  }

  
  deleteCustomer(Userid:string, id:string){
    this.db.doc(`users/${Userid}/customers/${id}`).delete(); 
  } 

  
  addCustomer(userId:string,name:string,years:number,income:number,category:string,ST:number){
    const customer = {name:name,years:years,income:income,category:category,ST:ST}; 
    this.userCollection.doc(userId).collection('customers').add(customer);
  }


  updateCustomer(userId:string,id:string,name:string,years:number,income:number,category:string,ST:number){
    this.db.doc(`users/${userId}/customers/${id}`).update(
      {
        name:name,
        years:years,
        income:income

      }
    )
  }
  




  constructor(private db:AngularFirestore) { }
}
