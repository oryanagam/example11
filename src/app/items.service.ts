import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import {AngularFirestore, AngularFirestoreCollection} from '@angular/fire/firestore';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class ItemsService {

  itemCollection:AngularFirestoreCollection;
  userCollection:AngularFirestoreCollection = this.db.collection('users'); 
  
  public getItems(userId){
    this.itemCollection = this.db.collection(`users/${userId}/items`, 
    ref => ref.orderBy('name', 'asc')); 
    return this.itemCollection.snapshotChanges()      
  } 

  ininventory(userId:string,id:string,name:string,price:number,inventory=true,ST:number){
    this.db.doc(`users/${userId}/items/${id}`).update(
      {
        name:name,
        price:price,
        inventory:inventory,
        ST:ST
      }
    )
  }

  save(userId:string,id:string,name:string,price:number,inventory:boolean,ST=2){
    this.db.doc(`users/${userId}/items/${id}`).update(
      {
        name:name,
        price:price,
        inventory:inventory,
        ST:ST
      }
    )
  }

  unsave(userId:string,id:string,name:string,price:number,inventory:boolean,ST=0){
    this.db.doc(`users/${userId}/items/${id}`).update(
      {
        name:name,
        price:price,
        inventory:inventory,
        ST:ST
      }
    )
  }



  deleteItem(Userid:string, id:string){
    this.db.doc(`users/${Userid}/items/${id}`).delete(); 
  } 

  addItem(userId:string,name:string,price:number,inventory:boolean,ST:string){
    const item = {name:name,price:price,inventory:inventory,ST:ST}; 
    this.userCollection.doc(userId).collection('items').add(item);
  }

  updateItem(userId:string,id:string,name:string,price:number,inventory:boolean,ST:string){
    this.db.doc(`users/${userId}/items/${id}`).update(
      {
        name:name,
        price:price,
        inventory:inventory,
        ST:ST
      }
    )
  }
  
  
  constructor(private db:AngularFirestore) { }



}