import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { ValidatorFn } from '@angular/forms';
import { Customer } from '../interfaces/customer';

@Component({
  selector: 'customerform',
  templateUrl: './customer-form.component.html',
  styleUrls: ['./customer-form.component.css']
})
export class CustomerFormComponent implements OnInit {

  @Input() name:string;  
  @Input() years:number;
  @Input() income:number;  
  @Input() category:string;
  @Input() ST:number;
  @Input() id:string; 
  @Input() formType:string;
  @Output() update = new EventEmitter<Customer>();
  @Output() closeEdit = new EventEmitter<null>();
  @Output() isError:boolean = false;
  

  updateParent(){
    let customer:Customer = {id:this.id, name:this.name, years:this.years, income:this.income, category:this.category ,ST:this.ST};
    if(this.years>24 || this.years<0){
      this.isError = true;
    }else{
      this.update.emit(customer); 
      if(this.formType == "Add customer"){
        this.name  = null;
        this.years = null;
        this.income = null;
      }
    }
  }


  tellParentToClose(){
    this.closeEdit.emit(); 
  }

  constructor() { }

  ngOnInit(): void {
  }

 
}
