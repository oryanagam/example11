import { PredictService } from './../predict.service';
import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth.service';
import { CustomersService } from '../customers.service';
import { Customer } from '../interfaces/customer';
import { AngularFirestore } from '@angular/fire/firestore';

@Component({
  selector: 'customers',
  templateUrl: './customers.component.html',
  styleUrls: ['./customers.component.css']
})
export class CustomersComponent implements OnInit {

  customers$; 
  customers:Customer[];
  userId:string; 
  editstate = [];
  addCustomerFormOpen = false;
  panelOpenState = false;
  //willReturn:boolean = false;
  //willNotReturn:boolean = false;
  //result;
  

  constructor(private customersService:CustomersService, public authService:AuthService, private predictService:PredictService,private db:AngularFirestore
    ) { }

  deleteCustomer(id:string){
    this.customersService.deleteCustomer(this.userId,id);
  }

  update(customer:Customer){
    this.customersService.updateCustomer(this.userId,customer.id,customer.name,customer.years,customer.income,customer.category, customer.ST);
  }


  save(customer:Customer){
    this.customersService.save(this.userId,customer.id ,customer.name, customer.years,customer.income,customer.category,customer.ST=2);
  }


  add(customer:Customer){
    this.customersService.addCustomer(this.userId,customer.name,customer.years,customer.income,customer.category="",customer.ST=0)
  }

  public predict(customer:Customer){
    this.predictService.predict(customer.years,customer.income).subscribe(
      res => {
        console.log(res);
        if(res > 0.5){
          console.log('yes');
          customer.category='will pay';
          customer.ST=1;
          console.log(customer.ST);
        } else {
          console.log('no');
          customer.category='will default';
          customer.ST=1;
        }
          console.log(customer.category);
        
      }
    )
  }





 /* classify(customer:Customer){
    this.predictService.predict(customer.years,customer.income).subscribe(
      res => {
        console.log(res);
        this.result = this.predictService[res];
      }
    )
  }*/

  
  ngOnInit(): void {
    this.authService.getUser().subscribe(
      user => {
        this.userId = user.uid;
        console.log(this.userId); 
        this.customers$ = this.customersService.getCustomers(this.userId); 
        
        this.customers$.subscribe(
          docs =>{
            console.log('init worked');           
            this.customers = [];
            for(let document of docs){
              const customer:Customer = document.payload.doc.data();
              customer.id = document.payload.doc.id; 
              this.customers.push(customer); 
            }
          }
        ) 
      }
    )

  }

}


